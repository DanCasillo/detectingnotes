//
//  AnalysisObserver.swift
//  DetectingNotes
//
//  Created by Danilo Casillo on 15/01/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit
import CoreML
import SoundAnalysis


//This class is used for observing the analysis of ML comparision
class AnalysisObserver: NSObject, SNResultsObserving {
    func request(_ request: SNRequest, didProduce result: SNResult) {
        //Check if result != nil and if the comparision make at least 1 result
        guard let result = result as? SNClassificationResult, let classification = result.classifications.first else {
            print("No result")
            return
        }
        
        print(classification.identifier)
        print(classification.confidence)
    }
    
    func request(_ request: SNRequest, didFailWithError error: Error) {
           print("The the analysis failed: \(error.localizedDescription)")
       }
       
       func requestDidComplete(_ request: SNRequest) {
           print("The request completed successfully!")
       }

}
