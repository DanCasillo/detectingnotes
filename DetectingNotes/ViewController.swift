//
//  ViewController.swift
//  DetectingNotes
//
//  Created by Danilo Casillo & Francesco Aroldo on 15/01/2020.
//  Copyright © 2020 Danilo Casillo & Francesco Aroldo. All rights reserved.
//

import UIKit
import CoreML
import SoundAnalysis
import AVFoundation

class ViewController: UIViewController {

    //Notes and Model definitions
    var notes : GuitarNotes!
    var model : MLModel!
    
    //File analyzer
    var audioFileAnalyzer : SNAudioFileAnalyzer!
    //Analysis observer
    var observer : AnalysisObserver!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notes = GuitarNotes()
        model = notes.model
    }
    
    //Capture audio Streaming -- N.B. To avoid errors, go to info.plist and insert Privacy about Microphone Usage
    @IBAction func playButtonTapped(_ sender: Any) {
        startCapture()
    }
    
    func startCapture(){
        let audioFileURL = URL(fileURLWithPath: "/Users/danilocasillo/Desktop/Bijoux.mp3") // URL of audio file to analyze (m4a, wav, mp3, etc.)
                    
        // Create a new audio file analyzer.
        do {
            
        audioFileAnalyzer = try SNAudioFileAnalyzer(url: audioFileURL)
        observer = AnalysisObserver()

        // Prepare a new request for the trained model.
        let request = try SNClassifySoundRequest(mlModel: model)
        try audioFileAnalyzer.add(request, withObserver: observer)
        
            audioFileAnalyzer.analyze()
        }
        catch {
            print(error)
        }
        
        
        
       
    }
      
}

