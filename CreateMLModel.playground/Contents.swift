
import Foundation
import CreateML

//let builder = try MLSoundClassifier(trainingData: notes)
//do
//{
//    try builder.write(to: URL(fileURLWithPath: "/Users/danilocasillo/Desktop"))
//    let urls = Bundle.main.
//}
//catch  {
//    print(error)
//}
var A = [URL]()
var B = [URL]()
var C = [URL]()
var D = [URL]()
var E = [URL]()
var F = [URL]()
var G = [URL]()
var notes = [String : [URL]]()
var fileManager = FileManager.default
var totFile : Int = 0
//For using it, change the fileURLWithPath parameter in the path of your Resources folder
let documentsURL = URL(fileURLWithPath: "/Users/danilocasillo/Desktop/ADA/DetectingNotes/DetectingNotes/CreateMLModel.playground/Resources")
do {
    
    let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
    //Adding each notes to related vector
    for file in fileURLs{
        switch (file.lastPathComponent.first!)
        {
        case "A" :
            A.append(file)
            break
        case "B" :
            B.append(file)
            break
        case "C" :
            C.append(file)
            break
        case "D" :
            D.append(file)
            break
        case "E" :
            E.append(file)
            break
        case "F" :
            F.append(file)
            break
        case "G" :
            G.append(file)
            break
        default:
            print("nothing")
        }
        totFile += 1
    }
    //Vector check
    print("A count: \(A.count)")
    print("B count: \(B.count)")
    print("C count: \(C.count)")
    print("D count: \(D.count)")
    print("E count: \(E.count)")
    print("F count: \(F.count)")
    print("G count: \(G.count)")
    print(totFile)
    
    //Inserting new pair in Dictionary "notes". updateValue add a pair if the Key "forKey" doesn't exist
    notes.updateValue(A, forKey: "A")
    notes.updateValue(B, forKey: "B")
    notes.updateValue(C, forKey: "C")
    notes.updateValue(D, forKey: "D")
    notes.updateValue(E, forKey: "E")
    notes.updateValue(F, forKey: "F")
    notes.updateValue(G, forKey: "G")
    
    //Dictionary check
    for key in notes.keys{
        print(key)
        for value in notes[key]!
        {
            print(value)
        }
    }
    
    let builder = try MLSoundClassifier(trainingData: notes)
    try builder.write(to: URL(fileURLWithPath: "/Users/danilocasillo/Desktop/ADA/DetectingNotes/DetectingNotes/DetectingNotes/GuitarNotes.mlmodel"))
    
} catch {
    print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
}



